# Wormy (a Nibbles clone)
# By Al Sweigart al@inventwithpython.com
# http://inventwithpython.com/pygame
# Released under a "Simplified BSD" license

from __future__ import division
import random, pygame, sys
from pygame.locals import *

FPS = 15
WINDOWWIDTH = 640 
WINDOWHEIGHT = 480 
CELLSIZE = 20
assert WINDOWWIDTH % CELLSIZE == 0, "Window width must be a multiple of cell size."
assert WINDOWHEIGHT % CELLSIZE == 0, "Window height must be a multiple of cell size."
CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)
CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)

#             R    G    B
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
DARKGREEN = (  0, 155,   0)
DARKGRAY  = ( 40,  40,  40)
BGCOLOR = BLACK

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

UP_2 = 'up2'
DOWN_2 = 'down2'
LEFT_2 = 'left2'
RIGHT_2 = 'right2'

HEAD = 0 # syntactic sugar: index of the worm's head
HEAD_2 = 0

#apple strategy
    #1. Apples are uniformly distributed and have infinite lifetime
    #2. Apples are uniformly distributed, but have short lifetime
    #3. Apples are uniformly distributed, but have a range of lifetimes
    #4. Combo of 1, 2, and 3 (note: apples do not move after being eaten)
    #5. Apples appear in a single quadrant
    #6. Apples appear in quadrants that change over time
    #7. Apple pattern of your own which makes centralized control superior.
STRATEGY = 7

NEIGHBORHOOD = 0

def main():
    global FPSCLOCK, DISPLAYSURF, BASICFONT

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    BASICFONT = pygame.font.Font('freesansbold.ttf', 18)
    pygame.display.set_caption('Snakes')

    showStartScreen()
    while True:
        runGame()
        showGameOverScreen()


def runGame():
    #apple distribution is first index
    #second index is for seeded random generator
    
    appleStrategy = [STRATEGY,1]

    #the time an apple has been on the screen
    appleTime = 0
    #keep track of missed apples
    applesMissed = 0
    #score of two snakes combind
    score = 0
    #count of ranges for number 3
    countLife = 0
    #count the time to change count combo for 4
    countTime = 0
    #count the combo to use on 4
    comboStrategy = [1,1]

    toggle = True

    time = 0

    findCorner1 = True
    findCorner2 = True
    
    # Set a random start point.
    startx = random.randint(5, CELLWIDTH - 10)
    starty = random.randint(5, CELLHEIGHT - 10)
    wormCoords = [{'x': startx,     'y': starty},
                  {'x': startx - 1, 'y': starty},
                  {'x': startx - 2, 'y': starty}]
    starty = starty + 5

    worm2Coords = [{'x': startx,     'y': starty},
                  {'x': startx - 1, 'y': starty},
                  {'x': startx - 2, 'y': starty}]
    
    direction = RIGHT
    direction2 = RIGHT_2

    # Start the apple in a random place.
    apple = getLocation(appleStrategy)
    apple2 = getLocation(appleStrategy)
    apple3 = getLocation(appleStrategy)
    apple4 = getLocation(appleStrategy)
    
    running = True
    while running: # main game loop

        #worm 1 going to corner
        if(findCorner1 == True):
            rightTurnX = 0
            rightTurnY = CELLHEIGHT - 2

            leftTurnX = CELLWIDTH - 3
            leftTurnY = CELLHEIGHT - 3
            if(direction == UP):
                direction = RIGHT
            if(direction == LEFT):
                direction = UP
            
            if(wormCoords[HEAD]['x'] == CELLWIDTH - 2 and direction == RIGHT):
                direction = DOWN
            if(wormCoords[HEAD]['y'] == CELLHEIGHT - 1 and direction == DOWN):
                direction = LEFT
            if(wormCoords[HEAD]['x'] == CELLWIDTH - 2 and wormCoords[HEAD]['y'] == CELLHEIGHT - 1):
                findCorner1 = False
        #check for wall for worm 1        
        if(findCorner1 == False):
            #turn up and then right
            if(wormCoords[HEAD]['x'] == rightTurnX and direction == LEFT):
                direction = UP
                
            if(wormCoords[HEAD]['y'] == rightTurnY and direction == UP):
                direction = RIGHT
                rightTurnY = rightTurnY - 2

            #turn up and then left
            if(wormCoords[HEAD]['x'] == leftTurnX and direction == RIGHT):
                direction = UP
                
            if(wormCoords[HEAD]['y'] == leftTurnY and direction == UP):
                direction = LEFT
                leftTurnY = leftTurnY - 2
                
        #worm 2 going to corner
        if(findCorner2 == True):
            rightTurn2X = CELLWIDTH - 2
            rightTurn2Y = 2 

            leftTurn2X = 1
            leftTurn2Y = 1

            if(direction2 == DOWN_2):
                direction2 = RIGHT_2
            if(direction2 == LEFT_2 and worm2Coords[HEAD_2]['y'] != 0):
                direction2 = DOWN_2
            if(worm2Coords[HEAD_2]['x'] == CELLWIDTH - 1 and direction2 == RIGHT_2):
                direction2 = UP_2
            if(worm2Coords[HEAD_2]['y'] == 0 and direction2 == UP_2):
                direction2 = LEFT_2
            if(worm2Coords[HEAD_2]['x'] == 1 and worm2Coords[HEAD_2]['y'] == 0):
                findCorner2 = False
                direction2 = DOWN_2

        if(findCorner2 == False):
            #turn down and then left
            if(worm2Coords[HEAD_2]['x'] == rightTurn2X and direction2 == RIGHT_2):
                direction2 = DOWN_2
                
            if(worm2Coords[HEAD_2]['y'] == rightTurn2Y and direction2 == DOWN_2):
                direction2 = LEFT_2
                rightTurn2Y = rightTurn2Y + 2
                
            #turn down and then right
            if(worm2Coords[HEAD_2]['x'] == leftTurn2X and direction2 == LEFT_2):
                direction2 = DOWN_2
                
            if(worm2Coords[HEAD_2]['y'] == leftTurn2Y and direction2 == DOWN_2):
                direction2 = RIGHT_2
                leftTurn2Y = leftTurn2Y + 2
                
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                terminate()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    terminate()

        # check if the worm has hit itself or the edge
        # checking worm 1
        if wormCoords[HEAD]['x'] == -1 or wormCoords[HEAD]['x'] == CELLWIDTH or wormCoords[HEAD]['y'] == -1 or wormCoords[HEAD]['y'] == CELLHEIGHT:
            drawScore(score)
            running = False # game over
        for wormBody in wormCoords[1:]:
            if (wormBody['x'] == wormCoords[HEAD]['x'] and wormBody['y'] == wormCoords[HEAD]['y']) or (wormBody['x'] == worm2Coords[HEAD_2]['x'] and wormBody['y'] == worm2Coords[HEAD_2]['y']):
                drawScore(score)
                running = False # game over

        # checking worm 2
        if worm2Coords[HEAD_2]['x'] == -1 or worm2Coords[HEAD_2]['x'] == CELLWIDTH or worm2Coords[HEAD_2]['y'] == -1 or worm2Coords[HEAD_2]['y'] == CELLHEIGHT:
            drawScore(score)
            running = False # game over
        for worm2Body in worm2Coords[1:]:
            if (worm2Body['x'] == worm2Coords[HEAD_2]['x'] and worm2Body['y'] == worm2Coords[HEAD_2]['y']) or (worm2Body['x'] == wormCoords[HEAD]['x'] and worm2Body['y'] == wormCoords[HEAD]['y']):
                drawScore(score)
                running = False # game over

        # move the worm by adding a segment in the direction it is moving
        # worm 1
        if direction == UP:
            newHead = {'x': wormCoords[HEAD]['x'], 'y': wormCoords[HEAD]['y'] - 1}
        elif direction == DOWN:
            newHead = {'x': wormCoords[HEAD]['x'], 'y': wormCoords[HEAD]['y'] + 1}
        elif direction == LEFT:
            newHead = {'x': wormCoords[HEAD]['x'] - 1, 'y': wormCoords[HEAD]['y']}
        elif direction == RIGHT:
            newHead = {'x': wormCoords[HEAD]['x'] + 1, 'y': wormCoords[HEAD]['y']}

        # worm 2
        if direction2 == UP_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'], 'y': worm2Coords[HEAD_2]['y'] - 1}
        elif direction2 == DOWN_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'], 'y': worm2Coords[HEAD_2]['y'] + 1}
        elif direction2 == LEFT_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'] - 1, 'y': worm2Coords[HEAD_2]['y']}
        elif direction2 == RIGHT_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'] + 1, 'y': worm2Coords[HEAD_2]['y']}
            
        #check for strategy
        if(appleStrategy[0] == 4):
            #check for which combo to use
            if(comboStrategy[0] == 4):
                comboStrategy[0] = 1
            newCoords = {}
            newCoords = checkIfWormHasEatenApple(toggle, findCorner1, findCorner2, wormCoords, worm2Coords, apple, apple2, apple3, apple4, comboStrategy, appleTime, applesMissed, countLife)
            #check to see if time's expired
            if(countTime == 150):
                countTime = 0
                comboStrategy[0] = comboStrategy[0] + 1
                
            countTime = countTime + 1
            wormCoords = newCoords['wormCoords']
            worm2Coords = newCoords['worm2Coords']
            apple = newCoords['apple']
            apple2 = newCoords['apple2']
            apple3 = newCoords['apple3']
            apple4 = newCoords['apple4']
            appleTime = newCoords['appleTime']
            applesMissed = newCoords['applesMissed']
            countLife = newCoords['countLife']
            toggle = newCoords['toggle']
            
        else:    
            newCoords = {}
            newCoords = checkIfWormHasEatenApple(toggle, findCorner1, findCorner2, wormCoords, worm2Coords, apple, apple2, apple3, apple4, appleStrategy, appleTime, applesMissed, countLife)
        
        wormCoords = newCoords['wormCoords']
        worm2Coords = newCoords['worm2Coords']
        apple = newCoords['apple']
        apple2 = newCoords['apple2']
        apple3 = newCoords['apple3']
        apple4 = newCoords['apple4']
        appleTime = newCoords['appleTime']
        applesMissed = newCoords['applesMissed']
        countLife = newCoords['countLife']
        findCorner1 = newCoords['findCorner1']
        findCorner2 = newCoords['findCorner2']
        toggle = newCoords['toggle']
        
        wormCoords.insert(0, newHead)
        worm2Coords.insert(0, newHead2)
        DISPLAYSURF.fill(BGCOLOR)
        drawGrid()
        drawWorm(wormCoords)
        draw2Worm(worm2Coords)
        drawApple(apple)
        drawApple(apple2)
        drawApple(apple3)
        #drawApple(apple4)

        #set score
        score = len(wormCoords) - 3 + len(worm2Coords) - 3 - applesMissed
        drawScore(score)
        pygame.display.update()
        FPSCLOCK.tick(FPS)
        time = pygame.time.get_ticks() / 1000
        print time

def checkIfWormHasEatenApple(toggle, findCorner1, findCorner2, wormCoords, worm2Coords, apple, apple2, apple3, apple4, appleStrategy, appleTime, applesMissed, countLife):
    newCoords = {}
    if (wormCoords[HEAD]['x'] == apple['x'] and wormCoords[HEAD]['y'] == apple['y']) or (wormCoords[HEAD]['x'] == apple2['x'] and wormCoords[HEAD]['y'] == apple2['y']) or (wormCoords[HEAD]['x'] == apple3['x'] and wormCoords[HEAD]['y'] == apple3['y']):
        # don't remove worm's tail segment
        # set a new apple somewhere
        apple = getLocation(appleStrategy) 
        apple2 = getLocation(appleStrategy)
        apple3 = getLocation(appleStrategy)
        apple4 = getLocation(appleStrategy)
        del worm2Coords[-1]
        findCorner1 = True
        findCorner2 = True
        toggle = not toggle
        appleTime = 0
    elif (worm2Coords[HEAD_2]['x'] == apple['x'] and worm2Coords[HEAD_2]['y'] == apple['y']) or (worm2Coords[HEAD_2]['x'] == apple2['x'] and worm2Coords[HEAD_2]['y'] == apple2['y']) or (worm2Coords[HEAD_2]['x'] == apple3['x'] and worm2Coords[HEAD_2]['y'] == apple3['y']):
        # set a new apple somewhere
        apple = getLocation(appleStrategy) 
        apple2 = getLocation(appleStrategy)
        apple3 = getLocation(appleStrategy)
        apple4 = getLocation(appleStrategy)
        del wormCoords[-1]
        findCorner1 = True
        findCorner2 = True
        toggle = not toggle
        appleTime = 0
    else:
        # remove worm's tail segment
        del wormCoords[-1] 
        del worm2Coords[-1]
        appleTime = appleTime + 1

    if(appleStrategy[0] == 2):
        if(appleTime == 50):
            # set a new apple somewhere
            apple = getLocation(appleStrategy) 
            apple2 = getLocation(appleStrategy)
            apple3 = getLocation(appleStrategy)
            apple4 = getLocation(appleStrategy)
            appleTime = 0
            applesMissed = applesMissed + 1
    elif(appleStrategy[0] == 3):
        #range 1
        if(appleTime == 100 and countLife == 0):
            # set a new apple somewhere
            apple = getLocation(appleStrategy) 
            apple2 = getLocation(appleStrategy)
            apple3 = getLocation(appleStrategy)
            apple4 = getLocation(appleStrategy)
            appleTime = 0
            applesMissed = applesMissed + 1
            countLife = 1
        #range 2
        if(appleTime == 75 and countLife == 1):
            # set a new apple somewhere
            apple = getLocation(appleStrategy) 
            apple2 = getLocation(appleStrategy)
            apple3 = getLocation(appleStrategy)
            apple4 = getLocation(appleStrategy)
            appleTime = 0
            applesMissed = applesMissed + 1
            countLife = 2
        #range 3
        if(appleTime == 25 and countLife == 2):
            # set a new apple somewhere
            apple = getLocation(appleStrategy) 
            apple2 = getLocation(appleStrategy)
            apple3 = getLocation(appleStrategy)
            apple4 = getLocation(appleStrategy)
            appleTime = 0
            applesMissed = applesMissed + 1
            countLife = 0
    if(appleStrategy[0] == 7):
        if(toggle):
            apple = getKnownLocation1()
            apple2 = getKnownLocation1()
            apple3 = getKnownLocation1()
            apple4 = getKnownLocation1()
            
        else:
            apple = getKnownLocation2()
            apple2 = getKnownLocation2()
            apple3 = getKnownLocation2()
            apple4 = getKnownLocation2()
    newCoords = { 'toggle': toggle, 'findCorner2': findCorner2, 'findCorner1': findCorner1, 'countLife': countLife, 'applesMissed': applesMissed, 'appleTime' : appleTime , 'wormCoords': wormCoords, 'worm2Coords': worm2Coords, 'apple': apple, 'apple2': apple2, 'apple3': apple3, 'apple4': apple4}
    return newCoords

def getLocation(strategy):
    if strategy[0] == 0:
        return getRandomLocation()
    elif strategy[0] == 1 or strategy[0] == 2 or strategy[0] == 3 or strategy[0] == 4:
        return getSeededRandomLocation(strategy)
    elif (strategy[0] == 5):
        return getSeededRandomLocationInQuad1(strategy)
    elif (strategy[0] == 6):
        time = pygame.time.get_ticks() / 1000

        if(time >= 0 and time <= 25):
            print 'quad 1'
            return getSeededRandomLocationInQuad1(strategy)
        if(time >= 26 and time <= 50):
            print 'quad 2'
            return getSeededRandomLocationInQuad2(strategy)
        if(time >= 51 and time <= 75):
            print 'quad 3'
            return getSeededRandomLocationInQuad3(strategy)
        if(time >= 76 and time <= 101):
            print 'quad 4'
            return getSeededRandomLocationInQuad4(strategy)
        elif(time > 101):
            return getSeededRandomLocationInQuad1(strategy)
    elif(strategy[0] == 7):
        return getSeededRandomLocationInQuad1(strategy)
    else:
        raise Exception(str(strategy[0]) + ' is not a know apple stratey')
    
def drawPressKeyMsg():
    pressKeySurf = BASICFONT.render('Press a key to play.', True, DARKGRAY)
    pressKeyRect = pressKeySurf.get_rect()
    pressKeyRect.topleft = (WINDOWWIDTH - 200, WINDOWHEIGHT - 30)
    DISPLAYSURF.blit(pressKeySurf, pressKeyRect)


def checkForKeyPress():
    if len(pygame.event.get(QUIT)) > 0:
        terminate()

    keyUpEvents = pygame.event.get(KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == K_ESCAPE:
        terminate()
    return keyUpEvents[0].key


def showStartScreen():
    titleFont = pygame.font.Font('freesansbold.ttf', 100)
    titleSurf1 = titleFont.render('snakes!', True, RED, DARKGREEN)
    titleSurf2 = titleFont.render('snakes!', True, RED)

    degrees1 = 0
    degrees2 = 0
    while True:
        DISPLAYSURF.fill(BGCOLOR)
        rotatedSurf1 = pygame.transform.rotate(titleSurf1, degrees1)
        rotatedRect1 = rotatedSurf1.get_rect()
        rotatedRect1.center = (WINDOWWIDTH / 2, WINDOWHEIGHT / 2)
        DISPLAYSURF.blit(rotatedSurf1, rotatedRect1)

        rotatedSurf2 = pygame.transform.rotate(titleSurf2, degrees2)
        rotatedRect2 = rotatedSurf2.get_rect()
        rotatedRect2.center = (WINDOWWIDTH / 2, WINDOWHEIGHT / 2)
        DISPLAYSURF.blit(rotatedSurf2, rotatedRect2)

        drawPressKeyMsg()

        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return
        pygame.display.update()
        FPSCLOCK.tick(FPS)
        degrees1 += 3 # rotate by 3 degrees each frame
        degrees2 += 7 # rotate by 7 degrees each frame


def terminate():
    pygame.quit()
    sys.exit()

def getKnownLocation1():
    return {'x': 3, 'y': 3}

def getKnownLocation2():
    return {'x': CELLWIDTH - 3, 'y': CELLHEIGHT - 3 }

def getKnowLocation(strategy):
    random.seed(1)
    return {'x': random.randint(0, CELLWIDTH - 1), 'y': random.randint(0, CELLHEIGHT - 1)}

def getSeededRandomLocation(strategy):
    random.seed(strategy[1])
    strategy[1] = strategy[1] + 1
    return {'x': random.randint(0, CELLWIDTH - 1), 'y': random.randint(0, CELLHEIGHT - 1)}

def getSeededRandomLocationInQuad1(strategy):
    random.seed(strategy[1])
    strategy[1] = strategy[1] + 1
    return {'x': random.randint(0, (CELLWIDTH / 2) - 1), 'y': random.randint(0, (CELLHEIGHT / 2) - 1)}

def getSeededRandomLocationInQuad4(strategy):
    random.seed(strategy[1])
    strategy[1] = strategy[1] + 1
    return {'x': random.randint((CELLWIDTH / 2), CELLWIDTH - 1), 'y': random.randint((CELLHEIGHT / 2), CELLHEIGHT - 1)}

def getSeededRandomLocationInQuad2(strategy):
    random.seed(strategy[1])
    strategy[1] = strategy[1] + 1
    return {'x': random.randint((CELLWIDTH / 2), CELLWIDTH - 1), 'y': random.randint(0, (CELLHEIGHT / 2) - 1)}

def getSeededRandomLocationInQuad3(strategy):
    random.seed(strategy[1])
    strategy[1] = strategy[1] + 1
    return {'x': random.randint(0, (CELLWIDTH / 2) - 1), 'y': random.randint((CELLHEIGHT / 2), (CELLHEIGHT - 1))}

def getRandomLocation():
    return {'x': random.randint(0, CELLWIDTH - 1), 'y': random.randint(0, CELLHEIGHT - 1)}


def showGameOverScreen():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 150)
    gameSurf = gameOverFont.render('Game', True, WHITE)
    overSurf = gameOverFont.render('Over', True, WHITE)
    gameRect = gameSurf.get_rect()
    overRect = overSurf.get_rect()
    gameRect.midtop = (WINDOWWIDTH / 2, 10)
    overRect.midtop = (WINDOWWIDTH / 2, gameRect.height + 10 + 25)

    DISPLAYSURF.blit(gameSurf, gameRect)
    DISPLAYSURF.blit(overSurf, overRect)
    drawPressKeyMsg()
    pygame.display.update()
    pygame.time.wait(500)
    checkForKeyPress() # clear out any key presses in the event queue

    while True:
        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return
    
def drawScore(score):
    scoreSurf = BASICFONT.render('Score 1: %s' % (score), True, WHITE)
    scoreRect = scoreSurf.get_rect()
    scoreRect.topleft = (WINDOWWIDTH - 360, 10)
    DISPLAYSURF.blit(scoreSurf, scoreRect)

def drawWorm(wormCoords):
    for coord in wormCoords:
        x = coord['x'] * CELLSIZE
        y = coord['y'] * CELLSIZE
        wormSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
        pygame.draw.rect(DISPLAYSURF, DARKGREEN, wormSegmentRect)
        wormInnerSegmentRect = pygame.Rect(x + 4, y + 4, CELLSIZE - 8, CELLSIZE - 8)
        pygame.draw.rect(DISPLAYSURF, GREEN, wormInnerSegmentRect)

def draw2Worm(wormCoords):
    for coord in wormCoords:
        x = coord['x'] * CELLSIZE
        y = coord['y'] * CELLSIZE
        wormSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
        pygame.draw.rect(DISPLAYSURF, DARKGREEN, wormSegmentRect)
        wormInnerSegmentRect = pygame.Rect(x + 4, y + 4, CELLSIZE - 8, CELLSIZE - 8)
        pygame.draw.rect(DISPLAYSURF, RED, wormInnerSegmentRect)

def drawApple(coord):
    x = coord['x'] * CELLSIZE
    y = coord['y'] * CELLSIZE
    appleRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
    pygame.draw.rect(DISPLAYSURF, RED, appleRect)


def drawGrid():
    for x in range(0, WINDOWWIDTH, CELLSIZE): # draw vertical lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (x, 0), (x, WINDOWHEIGHT))
    for y in range(0, WINDOWHEIGHT, CELLSIZE): # draw horizontal lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (0, y), (WINDOWWIDTH, y))
 

if __name__ == '__main__':
    main()
