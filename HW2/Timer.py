#!/usr/bin/python

import time

class Timer(object):

    def __init__(self):
        self.__time = time.time()

    def getTime(self):
        stopTime = time.time()
        elapsTime = (stopTime - self.__time) * 10000
        return elapsTime

    def reset(self):
        newTime = time.time()
        self.__time = newTime
