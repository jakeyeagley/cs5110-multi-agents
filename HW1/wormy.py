# Wormy (a Nibbles clone)
# By Al Sweigart al@inventwithpython.com
# http://inventwithpython.com/pygame
# Released under a "Simplified BSD" license

import random, pygame, sys
from pygame.locals import *

FPS = 15
WINDOWWIDTH = 640 * 2
WINDOWHEIGHT = 480 * 2
CELLSIZE = 20
assert WINDOWWIDTH % CELLSIZE == 0, "Window width must be a multiple of cell size."
assert WINDOWHEIGHT % CELLSIZE == 0, "Window height must be a multiple of cell size."
CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)
CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)

#             R    G    B
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
DARKGREEN = (  0, 155,   0)
DARKGRAY  = ( 40,  40,  40)
BGCOLOR = BLACK

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

UP_2 = 'up2'
DOWN_2 = 'down2'
LEFT_2 = 'left2'
RIGHT_2 = 'right2'

HEAD = 0 # syntactic sugar: index of the worm's head
HEAD_2 = 0

def main():
    global FPSCLOCK, DISPLAYSURF, BASICFONT

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    BASICFONT = pygame.font.Font('freesansbold.ttf', 18)
    pygame.display.set_caption('Snakes')

    showStartScreen()
    while True:
        runGame()
        showGameOverScreen()


def runGame():
    # Set a random start point.
    startx = random.randint(5, CELLWIDTH - 10)
    starty = random.randint(5, CELLHEIGHT - 10)
    wormCoords = [{'x': startx,     'y': starty},
                  {'x': startx - 1, 'y': starty},
                  {'x': startx - 2, 'y': starty}]
    starty = starty + 5

    worm2Coords = [{'x': startx,     'y': starty},
                  {'x': startx - 1, 'y': starty},
                  {'x': startx - 2, 'y': starty}]
    
    direction = RIGHT
    direction2 = RIGHT_2

    # Start the apple in a random place.
    apple = getRandomLocation()
    apple2 = getRandomLocation()
    apple3 = getRandomLocation()
    apple4 = getRandomLocation()

    running = True
    while running: # main game loop
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                terminate()
            elif event.type == KEYDOWN:
                # worm 1's controls
                if event.key == K_a and direction != RIGHT:
                    direction = LEFT
                elif event.key == K_d and direction != LEFT:
                    direction = RIGHT
                elif event.key == K_w and direction != DOWN:
                    direction = UP
                elif event.key == K_s and direction != UP:
                    direction = DOWN

                # worm 2's controls
                elif event.key == K_l and direction2 != LEFT_2:
                    direction2 = RIGHT_2    
                elif event.key == K_j and direction2 != RIGHT_2:
                    direction2 = LEFT_2
                elif event.key == K_i and direction2 != DOWN_2:
                    direction2 = UP_2
                elif event.key == K_k and direction2 != UP_2:
                    direction2 = DOWN_2
                    
                
                elif event.key == K_LEFT and direction != RIGHT and direction2 != RIGHT_2:
                    direction = LEFT
                    direction2 = LEFT_2
                elif event.key == K_RIGHT and direction != LEFT and direction2 != LEFT_2:
                    direction = RIGHT
                    direction2 = RIGHT_2
                elif event.key == K_UP and direction != DOWN and direction2 != DOWN_2:
                    direction = UP
                    direction2 = UP_2
                elif event.key == K_DOWN and direction != UP and direction2 != UP_2:
                    direction = DOWN
                    direction2 = DOWN_2
             
                if event.key == K_ESCAPE:
                    terminate()

        # check if the worm has hit itself or the edge
        # checking worm 1
        if wormCoords[HEAD]['x'] == -1 or wormCoords[HEAD]['x'] == CELLWIDTH or wormCoords[HEAD]['y'] == -1 or wormCoords[HEAD]['y'] == CELLHEIGHT:
            del wormCoords[-1]
            del wormCoords[-1]
            drawScore(len(wormCoords) - 3)
            running = False # game over
        for wormBody in wormCoords[1:]:
            if (wormBody['x'] == wormCoords[HEAD]['x'] and wormBody['y'] == wormCoords[HEAD]['y']) or (wormBody['x'] == worm2Coords[HEAD_2]['x'] and wormBody['y'] == worm2Coords[HEAD_2]['y']):
                del wormCoords[-1]
                del wormCoords[-1]
                drawScore(len(wormCoords) - 3)
                running = False # game over

        # checking worm 2
        if worm2Coords[HEAD_2]['x'] == -1 or worm2Coords[HEAD_2]['x'] == CELLWIDTH or worm2Coords[HEAD_2]['y'] == -1 or worm2Coords[HEAD_2]['y'] == CELLHEIGHT:
            del worm2Coords[-1]
            del worm2Coords[-1]
            draw2Score(len(worm2Coords) - 3)
            running = False # game over
        for worm2Body in worm2Coords[1:]:
            if (worm2Body['x'] == worm2Coords[HEAD_2]['x'] and worm2Body['y'] == worm2Coords[HEAD_2]['y']) or (worm2Body['x'] == wormCoords[HEAD]['x'] and worm2Body['y'] == wormCoords[HEAD]['y']):
                del worm2Coords[-1]
                del worm2Coords[-1]
                draw2Score(len(worm2Coords) - 3)
                running = False # game over

        # move the worm by adding a segment in the direction it is moving
        # worm 1
        if direction == UP:
            newHead = {'x': wormCoords[HEAD]['x'], 'y': wormCoords[HEAD]['y'] - 1}
        elif direction == DOWN:
            newHead = {'x': wormCoords[HEAD]['x'], 'y': wormCoords[HEAD]['y'] + 1}
        elif direction == LEFT:
            newHead = {'x': wormCoords[HEAD]['x'] - 1, 'y': wormCoords[HEAD]['y']}
        elif direction == RIGHT:
            newHead = {'x': wormCoords[HEAD]['x'] + 1, 'y': wormCoords[HEAD]['y']}

        # worm 2
        if direction2 == UP_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'], 'y': worm2Coords[HEAD_2]['y'] - 1}
        elif direction2 == DOWN_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'], 'y': worm2Coords[HEAD_2]['y'] + 1}
        elif direction2 == LEFT_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'] - 1, 'y': worm2Coords[HEAD_2]['y']}
        elif direction2 == RIGHT_2:
            newHead2 = {'x': worm2Coords[HEAD_2]['x'] + 1, 'y': worm2Coords[HEAD_2]['y']}
            
        # check if worm has eaten an apple
        if (wormCoords[HEAD]['x'] == apple['x'] and wormCoords[HEAD]['y'] == apple['y']) or (wormCoords[HEAD]['x'] == apple2['x'] and wormCoords[HEAD]['y'] == apple2['y']) or (wormCoords[HEAD]['x'] == apple3['x'] and wormCoords[HEAD]['y'] == apple3['y']):
            # don't remove worm's tail segment
            apple = getRandomLocation() # set a new apple somewhere
            apple2 = getRandomLocation()
            apple3 = getRandomLocation()
            apple4 = getRandomLocation()
            del worm2Coords[-1]
        elif (worm2Coords[HEAD_2]['x'] == apple['x'] and worm2Coords[HEAD_2]['y'] == apple['y']) or (worm2Coords[HEAD_2]['x'] == apple2['x'] and worm2Coords[HEAD_2]['y'] == apple2['y']) or (worm2Coords[HEAD_2]['x'] == apple3['x'] and worm2Coords[HEAD_2]['y'] == apple3['y']):    
            apple = getRandomLocation() # set a new apple somewhere
            apple2 = getRandomLocation()
            apple3 = getRandomLocation()
            apple4 = getRandomLocation()
            del wormCoords[-1]
        else:
            del wormCoords[-1] # remove worm's tail segment
            del worm2Coords[-1] 
            
        wormCoords.insert(0, newHead)
        worm2Coords.insert(0, newHead2)
        DISPLAYSURF.fill(BGCOLOR)
        drawGrid()
        drawWorm(wormCoords)
        drawWorm(worm2Coords)
        drawApple(apple)
        drawApple(apple2)
        drawApple(apple3)
        drawApple(apple4)
        drawScore(len(wormCoords) - 3)
        draw2Score(len(worm2Coords) - 3)
        pygame.display.update()
        FPSCLOCK.tick(FPS)

def drawPressKeyMsg():
    pressKeySurf = BASICFONT.render('Press a key to play.', True, DARKGRAY)
    pressKeyRect = pressKeySurf.get_rect()
    pressKeyRect.topleft = (WINDOWWIDTH - 200, WINDOWHEIGHT - 30)
    DISPLAYSURF.blit(pressKeySurf, pressKeyRect)


def checkForKeyPress():
    if len(pygame.event.get(QUIT)) > 0:
        terminate()

    keyUpEvents = pygame.event.get(KEYUP)
    if len(keyUpEvents) == 0:
        return None
    if keyUpEvents[0].key == K_ESCAPE:
        terminate()
    return keyUpEvents[0].key


def showStartScreen():
    titleFont = pygame.font.Font('freesansbold.ttf', 100)
    titleSurf1 = titleFont.render('snakes!', True, RED, DARKGREEN)
    titleSurf2 = titleFont.render('snakes!', True, RED)

    degrees1 = 0
    degrees2 = 0
    while True:
        DISPLAYSURF.fill(BGCOLOR)
        rotatedSurf1 = pygame.transform.rotate(titleSurf1, degrees1)
        rotatedRect1 = rotatedSurf1.get_rect()
        rotatedRect1.center = (WINDOWWIDTH / 2, WINDOWHEIGHT / 2)
        DISPLAYSURF.blit(rotatedSurf1, rotatedRect1)

        rotatedSurf2 = pygame.transform.rotate(titleSurf2, degrees2)
        rotatedRect2 = rotatedSurf2.get_rect()
        rotatedRect2.center = (WINDOWWIDTH / 2, WINDOWHEIGHT / 2)
        DISPLAYSURF.blit(rotatedSurf2, rotatedRect2)

        drawPressKeyMsg()

        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return
        pygame.display.update()
        FPSCLOCK.tick(FPS)
        degrees1 += 3 # rotate by 3 degrees each frame
        degrees2 += 7 # rotate by 7 degrees each frame


def terminate():
    pygame.quit()
    sys.exit()


def getRandomLocation():
    return {'x': random.randint(0, CELLWIDTH - 1), 'y': random.randint(0, CELLHEIGHT - 1)}


def showGameOverScreen():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 150)
    gameSurf = gameOverFont.render('Game', True, WHITE)
    overSurf = gameOverFont.render('Over', True, WHITE)
    gameRect = gameSurf.get_rect()
    overRect = overSurf.get_rect()
    gameRect.midtop = (WINDOWWIDTH / 2, 10)
    overRect.midtop = (WINDOWWIDTH / 2, gameRect.height + 10 + 25)

    DISPLAYSURF.blit(gameSurf, gameRect)
    DISPLAYSURF.blit(overSurf, overRect)
    drawPressKeyMsg()
    pygame.display.update()
    pygame.time.wait(500)
    checkForKeyPress() # clear out any key presses in the event queue

    while True:
        if checkForKeyPress():
            pygame.event.get() # clear event queue
            return

def drawScore(score):
    scoreSurf = BASICFONT.render('Score1: %s' % (score), True, WHITE)
    scoreRect = scoreSurf.get_rect()
    scoreRect.topleft = (WINDOWWIDTH - 360, 10)
    DISPLAYSURF.blit(scoreSurf, scoreRect)

def draw2Score(score):
    scoreSurf = BASICFONT.render('Score2: %s' % (score), True, WHITE)
    scoreRect = scoreSurf.get_rect()
    scoreRect.topright = (WINDOWWIDTH - 120, 10)
    DISPLAYSURF.blit(scoreSurf, scoreRect)

def drawWorm(wormCoords):
    for coord in wormCoords:
        x = coord['x'] * CELLSIZE
        y = coord['y'] * CELLSIZE
        wormSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
        pygame.draw.rect(DISPLAYSURF, DARKGREEN, wormSegmentRect)
        wormInnerSegmentRect = pygame.Rect(x + 4, y + 4, CELLSIZE - 8, CELLSIZE - 8)
        pygame.draw.rect(DISPLAYSURF, GREEN, wormInnerSegmentRect)


def drawApple(coord):
    x = coord['x'] * CELLSIZE
    y = coord['y'] * CELLSIZE
    appleRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
    pygame.draw.rect(DISPLAYSURF, RED, appleRect)


def drawGrid():
    for x in range(0, WINDOWWIDTH, CELLSIZE): # draw vertical lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (x, 0), (x, WINDOWHEIGHT))
    for y in range(0, WINDOWHEIGHT, CELLSIZE): # draw horizontal lines
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (0, y), (WINDOWWIDTH, y))


if __name__ == '__main__':
    main()
