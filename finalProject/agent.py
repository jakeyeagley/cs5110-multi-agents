#!/usr/bin/python

from Voter import Voter
import SocialChoice as socialChoice

class Agent(object):

    def __init__(self):
        self.__numOfPara = 0
        self.__priorityPara = []
        self.__priorityValues = []
        self.__mainPriorityWeight = 0
        self.__mainPriority = ''
        self.__ranks = {}
        self.__society = list()
        self.__candidates = []

    def setParameters(self,p):
        self.__priorityPara = p                # ['pay', 'start_date', location']
        self.__numOfPara = len(p)

    def setWeight(self,w):
        self.__mainPriorityWeight = w           # pay = prioriety of 10

        p = self.__mainPriorityWeight
        for i in range(self.__numOfPara):
            self.__priorityValues.append(p)     # start_date = 10 / 2
            p = p / 2.0                         # location = 10 / 2 / 2

    def setRanks(self,data,toReverseList):
        for value in range(self.__numOfPara):
            self.__ranks.update({self.__priorityValues[value] : sorted(data, key = lambda i: i[self.__priorityPara[value]],reverse=toReverseList[value])})

        self.__ranks = sorted(self.__ranks.iteritems(), reverse=True)

        # prints sorted lists of all three agents
        # print 'ranks: ', self.__ranks

        for i in range(len(data)):
            self.__candidates.append(i)


        ranks = []
        for j in range(self.__numOfPara):
            rank = []
            for i in range(len(data)):
                rank.append(self.__ranks[j][1][i]['id'])
            ranks.append(rank)

        
        for v in range(self.__numOfPara):
            self.__society.append(Voter())
            self.__society[v].setWeight(self.__ranks[v][0])
            self.__society[v].setRank(ranks[v])

    def runBucklin(self):
        return socialChoice.bucklin(self.__society, self.__candidates)
    
    def runBordaWithRunOff(self):
        return socialChoice.bordaWithRunOff(self.__society, self.__candidates)
    
    def runBorda(self):
        return socialChoice.borda(self.__society, self.__candidates)
                                                                                  
    def getRanks(self):
        return self.__ranks

    def getWeight(self):
        return self.__priorityValues

    def getCandidates(self):
        return self.__candidates
    
