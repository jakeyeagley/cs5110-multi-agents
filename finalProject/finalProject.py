#!/usr/bin/python

# user input
# user will input a priority to each parameter
# each agent (one for every parameter) will then determine a rank based off of that priority
# example:
# user's prioriety's: pay = 10, location = 5, start date = 1
# agent1's weights: pay = 10, location = 10 / 2, start date = 1/ 4
# agent2's weights: location = 5, pay = 5 / 2, start date = 5 / 4
# agent3's weights: start date = 1, pay = 1 / 2, location = 1 / 4
# the idea is that you are giving more voting power to the parameters with the highest priority

import random
from agent import Agent as agent
from Voter import Voter
import SocialChoice as socialChoice
import matplotlib.pyplot as plt

import collections

from math import cos, asin, sqrt

started = 1522980000 # started the project
myLocation = [41.7369, -111.8338] # Logan Utah

#https://stackoverflow.com/questions/41336756/find-the-closest-latitude-and-longitude?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295
    a = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p)*cos(lat2*p) * (1-cos((lon2-lon1)*p)) / 2
    return 12742 * asin(sqrt(a))

def closestLocation(data, v):
    return min(data, key=lambda p: distance(v['lat'],v['lng'],p['lat'],p['lng']))

def getSum(scores):
    sums = []
    for key, value in scores.iteritems():
        sums.append(value)
    return sum(sums)

def getAverage(scores):
    values = []
    for key, value in scores.iteritems():
        values.append(value)

    return sum(values) / len(values)

def getHighest(scores):
    values = []
    for key, value in scores.iteritems():
        values.append(value)

    return max(values)

def getWeightsAndRankings(method):
    sumWeights = []
    aveWeights = []
    highWeights = []
    rankings = []
    count = 0
    for score in method:
        rankings.append(sorted(score[1], key=score[1].get, reverse=True))
        #print 'rank for agent',count + 1, rankings[count]
        sumWeights.append(getSum(score[1]))
        #print 'with sum weight: ', sumWeights[count]
        aveWeights.append(getAverage(score[1]))
        #print 'with average weight: ', aveWeights[count]
        highWeights.append(getHighest(score[1]))
        #print 'with high weight: ', highWeights[count]
        count = count + 1

    weights = []
    weights.append(sumWeights)
    weights.append(aveWeights)
    weights.append(highWeights)

    return weights, rankings
    
def getSocietyAndCandidates(weights, rankings):
    society = list()
    for v in range(len(rankings)):
        society.append(Voter())
        society[v].setWeight(weights[v])
        society[v].setRank(rankings[v])

    candidates = []
    for i in range(len(formatedData)):
        candidates.append(i)

    return society, candidates  
        
def initializeJobData(length):
    
    jobInfo = []
    # initialize jobs data with random pay, location, and start date
    for i in range(length):
        jobInfo.append({"id": 0, "pay":0, "location" : {"lat":0,"lng":0}, "start_date": 0})
        jobInfo[i]['id'] = i
        jobInfo[i]['pay'] = random.randint(10000,150000)
        jobInfo[i]['start_date'] = random.randint(started + 1,1551772500)
        jobInfo[i]['location']['lat'] = round(random.uniform(30,50), 4)
        jobInfo[i]['location']['lng'] = round(random.uniform(-200,-20), 4)

    return jobInfo

def formatJobData(jobData, location, startTime, priorities):
    formated = []
    # format the data so that location is a single value and start time is based on time from now
    for i in range(len(jobData)):
        for key, value in jobData[i].iteritems():
            if(key == "id"):
                newId = value
            if(key == "pay"):
                newPay = value
            if(key == "location"):
                newLocation = distance(value['lat'],value['lng'], myLocation[0], myLocation[1])
            if(key == "start_date"):
                newStartDate = value - startTime

        formated.append({"id" : newId, "pay" : newPay, "location" : newLocation, "start_date" : newStartDate})        

    # needed for values that are better in higher value
    # example: pay we has a higher priority for larger values, but location is reversed
    reverseList = []                
    

    # setting priorities based of what the user inputed
    # if the priority of pay is the highest value then it will appear first in the list
    if(priorities[0] > priorities[1]):
        priorityPara = ['pay', 'location']
        reverseList = [True, False]
    else:
        priorityPara = ['location', 'pay']
        reverseList = [False, True]
        

    #sorting priorities so prioities are in order and reverseList follows
    if(priorities[0] < priorities[2] and priorities[1] < priorities[2]):
        priorityPara = ['start_date'] + priorityPara
        reverseList = [False] + reverseList
    elif(priorities[0] > priorities[2] and priorities[1] < priorities[2]):
        priorityPara = ['pay', 'start_date', 'location']
        reverseList = [True, False, False]
    else:
        priorityPara = priorityPara + ['start_date']
        reverseList = reverseList + [False]

    priorities = sorted(priorities, reverse=True)

    return formated, priorities, priorityPara, reverseList
    
if __name__ == '__main__':
    while True:
        simLen = input('enter simulation length: ')
        jobLength = input('how many jobs would you like to test? ')

        priorities = []
        
        priorities.append(input("pay's prioriety: "))
        priorities.append(input("location's prioriety: "))
        priorities.append(input("start date's prioriety: "))

        winnersBordaSecondStage = []
        winnersRunOffSecondStage = []
        winnersBucklinSecondStage = []

        stageOneWinners = []

        bestLocations = []
        bestPays = []
        bestStartingDates = []
        
        while simLen:
            simLen = simLen - 1

            # get data
            jobData = initializeJobData(jobLength)
            formatedData, priorities, priorityPara, reverseList = formatJobData(jobData, myLocation, started, priorities)

            agents = list()
            for i in range(len(priorities)):
                agents.append(agent())
                agents[i].setParameters(priorityPara)
                agents[i].setWeight(priorities[0])
                agents[i].setRanks(formatedData, reverseList)
                print 'ranks: ', agents[i].getRanks()
                print 'weight: ', agents[i].getWeight()
                ###print 'agent',i+1,"'s priority is", priorityPara[i], ', with a value of ', priorities[0]
                # rotating priotities to give each agent a different main priority
                priorities.append(priorities[0])                        
                priorities.remove(priorities[0])
                reverseList.append(reverseList[0])
                reverseList.remove(reverseList[0])
                priorityPara.append(priorityPara[0])
                priorityPara.remove(priorityPara[0])


            bucklin = []
            borda = []
            bordaWithRunOff = []

            bordaWinners = []
            bucklinWinners = []
            runOffWinners = []
                
            for i in range(len(agents)):
                borda.append(agents[i].runBorda())
                bucklin.append(agents[i].runBucklin())
                bordaWithRunOff.append(agents[i].runBordaWithRunOff())

                stageOneWinners.append(borda[i][0])
                bordaWinners.append(borda[i][0])
                stageOneWinners.append(bucklin[i][0])
                bucklinWinners.append(bucklin[i][0])
                stageOneWinners.append(bordaWithRunOff[i][0])
                runOffWinners.append(bordaWithRunOff[i][0])

            print 'stage 1 winners: ', stageOneWinners
            print 'borda: ', bordaWinners
            print 'bucklin: ', bucklinWinners
            print 'runOff: ', runOffWinners
            print 'bucklin 1st stage for 3 agents: ', bucklin
            print 'borda 1st stage for 3 agents: ', borda
            print 'runoff 1st stage for 3 agents: ', bordaWithRunOff

            
            weightsBorda, rankingsBorda = getWeightsAndRankings(borda)
            weightsRunOff, rankingsRunOff = getWeightsAndRankings(bordaWithRunOff)
            weightsBucklin, rankingsBucklin = getWeightsAndRankings(bucklin)
            
            # weights for combinding ranks
            # 0 = sum
            # 1 = average
            # 2 = highest value

            winners = []

            for i in range(len(agents)):
                weightsBorda, rankingsBorda = getWeightsAndRankings(borda)
                weightsRunOff, rankingsRunOff = getWeightsAndRankings(bordaWithRunOff)
                weightsBucklin, rankingsBucklin = getWeightsAndRankings(bucklin)
                #print 'i: ', i
                society, candidates = getSocietyAndCandidates(weightsBorda[i], rankingsBorda)

                #print 'borda weights: ', weightsBorda[i]
                #print 'borda rankings: ', rankingsBorda

                #print '#####Borda with agents#####'
                winner, temp = socialChoice.bucklin(society, candidates)
                #print 'final with bucklin and borda for agents: ', winner, temp
                #print 'Bucklin: ', winner

                winners.append(winner)
                winnersBucklinSecondStage.append(winner)
                
                winner, temp = socialChoice.borda(society, candidates)
                #print 'final with borda and borda for agents: ', winner, temp
                #print 'Borda: ', winner

                winners.append(winner)
                winnersBordaSecondStage.append(winner)
                
                winner, temp = socialChoice.bordaWithRunOff(society, candidates)
                #print 'final with runoff and borda for agents: ', winner, temp
                #print 'RunOff: ', winner

                winners.append(winner)
                winnersRunOffSecondStage.append(winner)
                
                # break
                society, candidates = getSocietyAndCandidates(weightsRunOff[i], rankingsRunOff)

                #print 'runOff weights: ', weightsRunOff[i]
                #print 'runOff rankings: ', rankingsRunOff

                #print '#####RunOff with agents#####'
                winner, temp = socialChoice.bucklin(society, candidates)    
                #print 'final with bucklin and runoff for agents: ', winner, temp
                #print 'Bucklin: ', winner

                winners.append(winner)
                winnersBucklinSecondStage.append(winner)
                
                winner, temp = socialChoice.borda(society, candidates)
                #print 'final with borda and runoff for agents: ', winner, temp
                #print 'Borda: ', winner      

                winners.append(winner)
                winnersBordaSecondStage.append(winner)
                
                winner, temp = socialChoice.bordaWithRunOff(society, candidates)
                #print 'final with runoff and runoff for agents: ', winner, temp
                #print 'RunOff: ', winner

                winners.append(winner)
                winnersRunOffSecondStage.append(winner)
                
                # break
                society, candidates = getSocietyAndCandidates(weightsBucklin[i], rankingsBucklin)
                
                #print 'bucklin weights: ', weightsBucklin[i]
                #print 'bucklin rankings: ', rankingsBucklin
                
                winner, temp = socialChoice.bucklin(society, candidates)
                #print '#####Bucklin for agents######'
                #print 'final with bucklin and Bucklin for agents: ', winner, temp
                #print 'Bucklin: ', winner

                winners.append(winner)
                winnersBucklinSecondStage.append(winner)
                
                winner, temp = socialChoice.borda(society, candidates)
                #print 'final with borda and Bucklin for agents: ', winner, temp
                #print 'Borda: ', winner

                winners.append(winner)
                winnersBordaSecondStage.append(winner)
                
                winner, temp = socialChoice.bordaWithRunOff(society, candidates)
                #print 'final with runoff and Bucklin for agents: ', winner, temp
                #print 'RunOff: ', winner

                winners.append(winner)
                winnersRunOffSecondStage.append(winner)

            pay = sorted(formatedData, key = lambda i: i['pay'],reverse=True)
            start = sorted(formatedData, key = lambda i: i['start_date'], reverse=False)
            location = sorted(formatedData, key = lambda i: i['location'], reverse = False)
            
            #print 'top 3 salaries:'
            #print pay[0]['id']
            #print pay[1]['id']
            #print pay[2]['id']
            bestPays.append(pay[0]['id'])

            #print 'top 3 locations:'
            #print location[0]['id']
            #print location[1]['id']
            #print location[2]['id']
            bestLocations.append(location[0]['id'])

            #print 'top 3 start dates:'
            #print start[0]['id']
            #print start[1]['id']
            #print start[2]['id']
            bestStartingDates.append(start[0]['id'])

            #print 'winners: ', winners
            #print 'second stages: '
            #print 'bucklin: '
            #print winnersBucklinSecondStage 
            #print 'borda: '
            #print winnersBordaSecondStage
            #print 'runOff: '
            #print winnersRunOffSecondStage

        #fig1 = plt.figure(1)
        #fig1.suptitle('stage 1 winners vs. stage 2 borda winners')
        #plt.xlabel('stage 1 winners')
        #plt.ylabel('stage 2 borda winners')
        #plt.grid()
        #plt.scatter(stageOneWinners, winnersBordaSecondStage)
        #fig1.show()

        frequencyBorda = collections.Counter(winnersBordaSecondStage)
        frequencyRunOff = collections.Counter(winnersRunOffSecondStage)
        frequencyBucklin = collections.Counter(winnersBucklinSecondStage)

        frequencyBestPays = collections.Counter(bestPays)

        x = []
        for i in range(len(frequencyBorda)):
            x.append(i)

        keysBorda = frequencyBorda.keys()
        keysRunOff = frequencyRunOff.keys()
        keysBucklin = frequencyBucklin.keys()

        w = 0.3
        fig = plt.figure('Stage 2 winners with stage 1 winners')
        x = []
        for i in range(len(frequencyBorda)):
            x.append(i - w)
        bar1 = plt.bar(x, frequencyBorda.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBorda.values())):
            bar1[i].set_color('r')
        x = []
        for i in range(len(frequencyBucklin)):
            x.append(i)
        bar2 = plt.bar(x, frequencyBucklin.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBucklin.values())):
            bar2[i].set_color('g')
        x = []
        for i in range(len(frequencyRunOff)):
            x.append(i + w)
        bar3 = plt.bar(x, frequencyRunOff.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyRunOff.values())):
            bar3[i].set_color('y')
        x = []
        for i in range(len(frequencyBestPays)):
            x.append(i - 2 * w)
        bar4 = plt.bar(x, frequencyBestPays.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBestPays.values())):
            bar4[i].set_color('#0000FF')

        plt.legend((bar1[0], bar2[0], bar3[0], bar4[0]), ('borda', 'bucklin', 'runOff', 'winners'))
        plt.xlabel('id number')
        plt.ylabel('frequency')
        fig.show()

        #####################################
        
        frequencyBestPays = collections.Counter(bestPays)
        frequencyBestLocations = collections.Counter(bestLocations)
        frequencyBestStartingDates = collections.Counter(bestStartingDates)
        
        w = 0.3
        figBest = plt.figure('bests')
        x = []
        for i in range(len(frequencyBestPays)):
            x.append(i - w)
        bar1 = plt.bar(x, frequencyBestPays.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBestPays.values())):
            bar1[i].set_color('#00FF00')
        x = []
        for i in range(len(frequencyBestLocations)):
            x.append(i)
        bar2 = plt.bar(x, frequencyBestLocations.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBestLocations.values())):
            bar2[i].set_color('#FFFF00')
        x = []
        for i in range(len(frequencyBestStartingDates)):
            x.append(i + w)
        bar3 = plt.bar(x, frequencyBestStartingDates.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBestStartingDates.values())):
            bar3[i].set_color('#0000FF')

        plt.legend((bar1[0], bar2[0], bar3[0]), ('pay', 'location', 'starting date'))
        plt.xlabel('id number')
        plt.ylabel('frequency')
        figBest.show()

        #######################################

        figPay = plt.figure('top pays')
        x = []
        for i in range(len(frequencyBestPays)):
            x.append(i)
        bar1 = plt.bar(x, frequencyBestPays.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBestPays.values())):
            bar1[i].set_color('#00FF00')

        plt.xlabel('id number')
        plt.ylabel('frequency')
        figPay.show()

        ######################################

        frequency = collections.Counter(stageOneWinners)
        figStage1 = plt.figure('stage 1 winners with top pay')
        x = []
        for i in range(len(frequencyBestPays)):
            x.append(i - (w / 2))
        bar1 = plt.bar(x, frequencyBestPays.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequencyBestPays.values())):
            bar1[i].set_color('#00FF00')
        x = []
        for i in range(len(frequency)):
            x.append(i + (w / 2))
        bar2 = plt.bar(x, frequency.values(), width=w, align='center', alpha=0.5)
        for i in range(len(frequency.values())):
            bar2[i].set_color('#0000FF')

        plt.legend((bar1[0], bar2[0]), ('top pays', 'stage 1 winners'))
        plt.ylabel('frequency')
        figStage1.show()
        #####################################

        #####################################
        frequency = collections.Counter(stageOneWinners)
        x = []
        for i in range(len(frequency)):
            x.append(i)
        keys = frequency.keys()
        fig5 = plt.figure('stage 1 winners')
        plt.bar(x, frequency.values(), align='edge', alpha=0.5)
        plt.xticks(x, keys)
        plt.xlabel('id number')
        plt.ylabel('frequency')
        fig5.show()

        raw_input("PRESS ANY KEY TO CONTINUE.")
        plt.close('all')

# agent
# decide what's most important parameters based of priorities (money, start date, or location)
# sort 
# give a rankings of each (each agent will need priorities on parameters)

# voter lingo for agent decisions
# voter = parameter
# canadidates = jobs (or id number)
# number of voters = number of parameters
# weight = parameter's prioriety
# rank will be set on highest value for money, closest value for location, and closet to idle start time 
# setting the rank function will be a sorted list of  certain parameter
# example: highest pay = highest rank for parameter(voter) pay

# to combind the 3 voter/parameter agents the weights will be determined 3 different ways.
# By sum of all points
# By highest value of the winner
# By the average 
