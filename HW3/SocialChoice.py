#!/user/bin/pythons

#Voting methods implemented: Borda, Bucklin, and Borda with runoff (my own creation)
#Ties are decided by order of candidates, ex) if ['a','b'] are the candidate list a would win the tie breaker

from Voter import Voter

def randomSociety():
    candidates = input("enter an array of canadidtes: ")
    numberOfVoters = input("number of voters: ")

    #add voters to society and set ranks then set weights
    society = list()
    for v in range(numberOfVoters):
        society.append(Voter())
        print "set weight for voter number ",v + 1, ": "
        weight = input()
        society[v].setWeight(weight)
        society[v].setRank(candidates)
        print "rank: ", society[v].getRank()

    return society, candidates
    
def main():
    while True:
        society, candidates = randomSociety()

        winner, score = bucklin(society,candidates)
        print "score with bucklin: ", score
        print "the winner was: ", winner
        winner, score = borda(society, candidates)
        print 'score with borda: ', score
        print 'the winner was: ', winner
        winner, score = bordaWithRunOff(society,candidates)
        print 'score with borda with run off: ', score
        print 'the winner was: ', winner

def borda(society,candidates):
    numOfCandidates = len(candidates)

    #keep track of scores for each candidate
    sumForBorda = []

    #initialize dictionary of scores for candidates
    score = {}
    for c in range(numOfCandidates):
        score.update({candidates[c]:0})
        #print score.items()[c]

    #go through each voters rank
    for voter in society:
        #go through each candidate
        for canIndex in range(numOfCandidates):
            #go through each candidate's score
            for canKey, canScore in score.items():
                #update score to candidates c's score
                if(canKey == voter.getCandidate(canIndex)):
                    #add canidates previous score to the index location of the candidate times the weight of the voter
                    score[canKey] = canScore + (canIndex * voter.getWeight())
                    break
        #print 'score after voter: ', score

    #print "final score: ", score
    max1 = max(score.items(), key=lambda x: x[1])
    del score[max1[0]]
    max2 = max(score.items(), key=lambda x: x[1])
    score.update({max1[0]:max1[1]})
    
    winner = min(score.items(), key=lambda x: x[1])
    return winner[0], score

def bucklin(society,candidates):
    numOfVoters = len(society)
    numOfCandidates = len(candidates)
    
    #initialize dictionary of scores
    score = {}
    for c in range(numOfCandidates):
        score.update({candidates[c]:0})

    #check column with k=1
    for k in range(numOfCandidates):
        for voter in society:
            #print 'voter rank: ', voter.getRank()
            #print 'column ',k,': ', voter.getCandidate(k)

            #add up score of a candidate appearance in column k
            score[voter.getCandidate(k)] = score[voter.getCandidate(k)] + voter.getWeight()
            #print 'score: ', score

        #check for winner by getting the top two max scores, if they are equal repeat loop
        max1 = max(score.items(), key=lambda x: x[1])
        del score[max1[0]]
        max2 = max(score.items(), key=lambda x: x[1])
        score.update({max1[0]:max1[1]})

        if(max1[1] != max2[1]):
            return max1[0], score
            break
        #print 'there was no clear winner after column ', k

        #if no clear winner, repeat with k++
            
    return max1, score
    
def bordaWithRunOff(society,candidates):
    #initiale score from borda
    doNothing, score = borda(society,candidates)
    finalRanking = {}

    #remove last place from borda score and re-compute the borda score again 
    for i in range(len(candidates) - 2):
        #remove max value
        remove = max(score.items(), key=lambda x: x[1])
        #add removed value to final ranking
        finalRanking.update({remove[0]:remove[1]})
        del score[remove[0]]
        
        #for display
        #print "score now: ", score

        #remove candidate from society
        for s in society:
            s.removeCandidate(remove[0])
            #print 'rank of voter:', s.getCandidates()
            
        #remove candidate from list of candidates
        index = candidates.index(remove[0])
        del candidates[index]
        #get new score
        doNothing, score = borda(society,candidates)

    finalRanking.update(score)
    #print 'final ranking: ', finalRanking

    max1 = max(finalRanking.items(), key=lambda x: x[1])
    del finalRanking[max1[0]]
    max2 = max(finalRanking.items(), key=lambda x: x[1])
    finalRanking.update({max1[0]:max1[1]})

    
    winner = min(finalRanking.items(), key=lambda x: x[1])
    return winner[0], finalRanking
    
if __name__ == '__main__':
    main()    
