#!/user/bin/python

import random

class Voter(object):

    def __init__(self):
        self.__weight = 1
        self.candidates = []

    def setWeight(self, w):
        self.__weight = w

    def setCandidates(self, c):
        self.__candidates = c

    def setRank(self, c):
        self.__candidates = random.sample(c, len(c))

    def getWeight(self):
        return self.__weight

    def getCandidates(self):
        return self.__candidates

    def getCandidate(self, index):
        return self.__candidates[index]

    def getRank(self):
        return self.__candidates
            
    def removeCandidate(self, can):
        index = self.__candidates.index(can)
        del self.__candidates[index]
