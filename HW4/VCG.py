#!/user/bin/pythons

#parameters
# number of bidders
# pattern of bids Ex) .5, .45, .2, .1, .1
# number of clicks
# number of advertising slots a.k.a number of winners

def main():
    while True:
        #get input
        #array of bidders EX) [.5,.4,.3,.2,.1]
        pattern = input("bid pattern: ")
        #array of click per slot EX) [500,300,100]
        clicks = input("number of clicks for each slot: ")
        #determine winners
        numOfWinners = len(clicks)

        #append 0 for C3 - 0 (base case)
        clicks.append(0)
        #append 0 in case numBidders = numWinners
        pattern.append(0)
        
        #values of numbers that are added up based on winning location
        values = []
        for i in reversed(range(numOfWinners)):
            #calculate Pnw+1 where nw is number of winner
            Pn = pattern[i + 1]
            #find (Cnw+1) - Cnw and call it displacement 
            displacement = (clicks[i] - clicks[i + 1])
            #find Pnw+1 * displacement and call it value
            value = Pn * displacement
            values.append(value)

        #add up all previous values for i < nw
        payments = []
        for i in reversed(range(numOfWinners)):
            payments.append(sum(values))
            del values[i]

        print 'payments: '
        print payments
    pass
if __name__ == '__main__':
    main()
